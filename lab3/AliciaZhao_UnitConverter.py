# Convert to bytes
"""
Objective: Create a Python program that takes a number as input from the user and converts it into kilobytes (KB), kibibytes (kiB), megabytes (MB), and mebibytes (MiB).

Instructions:

Begin by prompting the user to enter a numerical value. Only integers are allowed (123), not floating point (1.23)

Convert the user's input into the following units:

Kilobytes (KB): 1 kilobyte is equal to 1000 bytes.
Kibibytes (kiB): 1 kibibyte is equal to 1024 bytes.
Megabytes (MB): 1 megabyte is equal to 1000 kilobytes.
Mebibytes (MiB): 1 mebibyte is equal to 1024 kibibytes.
Display the converted values along with appropriate labels to clearly indicate the unit of measurement (e.g., "X kilobytes," "Y kibibytes," etc.). You may use formatted output to make the results easy to read.

Ensure that your program handles errors gracefully. If the user enters invalid input (e.g., non-numeric input or negative values), provide a clear error message and allow the user to try again.

Test your program with various input values to ensure it produces correct and accurate conversions.
"""
import sys
def main():
    # test if input is an Integer

    num = input("Please enter a number in bytes: ")

    if num.isnumeric() == False:
        sys.exit("entered value was not an integer"); 
    if int(num) < 0:
        sys.exit("Entered value was not positive")
    else:
        convert_to_bytes(num)

def convert_to_bytes(num):

    # 1 kilobyte = 1000 bytes.
    kb = num / 1000
    # 1 kibibyte = 1024 bytes.
    kib = num / 1024
    # 1 megabyte = 1000 kilobytes.
    mb = kb / 1000
    # 1 mebibyte = 1024 kibibytes.
    mib = kib / 1024

    return "Conversions: \n" + kb + " kilobytes \n" + kib + " kibibytes \n" + mb + " megabytes \n" + mib + " mebibyte \n"

print(convert_to_bytes);
