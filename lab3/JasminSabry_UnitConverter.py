#Jasmin Sabry
#unit converter program

def unitConverter(num):
    #convert our units
    kb = num / 1000
    kib = num / 1024
    mb = num / 1000000
    mib = num / 1048576
    print("Conversions:")
    print(f"{num:,.0f} bytes")
    print(f"{kb:,.6f} kilobytes")
    print(f"{kib:,.6f} kibibytes")
    print(f"{mb:,.6f} megabytes")
    print(f"{mib:,.6f} mebibytes")


print("Enter a number to convert bytes to kilobytes, kibibytes, megabytes, and mebibytes.")
while True:
    print("Enter quit to stop: ")
    num = input("Enter a number (total bytes): ")
    if num == 'quit':
        break
    elif num.isdigit():
        num = int(num)
        unitConverter(num)
    elif '-' in str(num):
        print("Enter a positive #")
    else:
        print("Enter a number.")
