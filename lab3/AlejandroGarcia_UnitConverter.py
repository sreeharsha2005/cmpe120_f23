# Welcome to Alejandro Garcia's Python Units Converter
'''
The purpose of this program is to take an input value (numerical/integer value > 0) from the user and show 
the equivalent value in the form of:
 Bytes (B): Value will be entered in bytes
 Kilobtyes (KB): 1 kilobyte is equivalent to 1000 bytes
 Kibibytes (kiB): 1 kibibyte is equivalent to 1024 bytes
 Megabytes (MB): 1 megabyte is equivalent to 1000 kilobytes
 Mebibytes (miB): 1 mebibyte is equivalent to 1024 kibibytes
'''

class UnitsConverter:

    def convertUnits(self, num: int): #function used to convert the integer values
        # See comments above for conversions
        kiloByte = num / 1000
        kiBiByte = num / 1024
        megaByte = kiloByte / 1000
        mebiByte = kiBiByte / 1024

        # print statements to display all of the unit conversions
        print("Conversion Output:")
        print(num, "bytes")
        print(kiloByte, "kilobytes (KB)")
        print(kiBiByte, "kibibytes (kiB)")
        print(megaByte, "megabytes (MB)")
        print(mebiByte, "mebiBytes (miB)")

def main():
    # Use while loop to take input from user and make sure that value is a positive integer 
    while True:
        # prompt the user to enter a number for the unit conversions
        print("Enter a number: ")
        bytes = input()

        if bytes.isnumeric(): # check to see if the entered value is an integer
            if int(bytes) > 0: # check to see if the integer is greater than 0
                UnitsConverter().convertUnits(int(bytes)) # if True, make conversions and exit while loop / program
                break
            else:
                print("Enter a number greater than 0") # Prompt user to enter a number that is greater than 0
        else:
            print("Entered value is not numerical") # if the entered value is not an integer, the user will be prompted to enter an integer greater than 0

main()