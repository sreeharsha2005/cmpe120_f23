def main():   # define a function named "main"
    while True:
        try:
            user_input = input("Please enter an integer number: ")
            if user_input.isdigit():     # user_input.isdigit() to check if the input is valid integer
                user_input = int(user_input)
                python_units_converter(user_input)
                break                   #  breakout infinite loop since entering valid input
            else:
                print("Invalid number. Please enter an integer number only.")
        except ValueError:              # this catch Value Error if the input is not valid integers
            print("Invalid input. Please enter an integer number.")

def python_units_converter(bytes_value):
    kilobytes = bytes_value / 1000
    kibibytes = bytes_value / 1024
    megabytes = bytes_value / 1000000
    mebibytes = bytes_value / 1024000

    print(f"{bytes_value} bytes can be expressed as")
    print(f"{kilobytes} kilobytes ")
    print(f"{kibibytes} kibibytes ")
    print(f"{megabytes} megabytes ")
    print(f"{mebibytes} mebibytes ")

main()    # call function directly to execute


