'''
1) error handaling
2) class implementation with methods
3) formatting output prints
'''

class UnitsConverter:
    def convert_to_units(self, num: int): # Define a method within the class to convert units based on input 'num'
        kiloByte = num / 1000  # Calculate kilobytes (KB) by dividing 'num' by 1000
        kiBiByte = num / 1024 # Calculate kibibytes (kiB) by dividing 'num' by 1024
        megabyte = num / (1000 * 1000) # Calculate megabytes (MB) by dividing 'num' by 1,000,000 (1000 * 1000)
        mebibyte = num / (1024 * 1024) # Calculate mebibytes (MiB) by dividing 'num' by 1,048,576 (1024 * 1024)


        print("Conversions:") # Print the conversion results
        print(f"{num} bytes") # Print the original 'num' in bytes
        print('%.2f kilobytes' %(kiloByte)) # Print 'kiloByte' formatted with two decimal places as kilobytes
        print('%.2f kiBiByte' %(kiBiByte))  # Print 'kiBiByte' formatted with two decimal places as kibibytes
        print('%.2f megabyte' %(megabyte))  # Print 'megabyte' formatted with two decimal places as megabytes
        print('%.2f mebibyte' %(mebibyte))  # Print 'mebibyte' formatted with two decimal places as mebibytes

def main(): # Define main
    converter = UnitsConverter()

while True: # Start an infinite loop
    print("enter a number:")
    input_var = input()
    if input_var.isnumeric(): # Check if the input is a numeric string
        if int(input_var) > 0: # Check if the integer value of 'input_var' is greater than 0
         UnitsConverter().convert_to_units(int(input_var)) # Create a UnitsConverter instance and perform unit conversions with 'input_var' as the argument.
         break
    else:
        print("entered value is not numerical") # print "entered value is not numerical"
else:
    print("entered value is not numerical") # print "entered value is not numerical"

main()


"changed"