# This is a sample Python script.

# Press Shift+F6 to execute it or replace it with your code.

def convert_to_units( num: int):
    kiloByte = num / 1000
    kiBiByte = num / 1024
    megaByte = kiloByte / 1000
    mebiByte = kiBiByte / 1024
    print("\nConversions: ")
    print("{:,} bytes".format(num))
    print("%d kilobytes" %(kiloByte))
    print("%0.3f kibibytes" %(kiBiByte))
    print("%0.1f megaByte" %(megaByte))
    print("%0.6f mebiByte" %(mebiByte))

while True:
    number = input("Enter a number: ")
    if number.isnumeric():
        if int(number) >= 0:
            convert_to_units(int(number))
            break
    else:
        print("Entered value is not numberical")
    


