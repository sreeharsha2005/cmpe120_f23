def units_converter(number):
    print("Conversions:")
    print(str(number)," bytes")
    kilo_bytes=number//1000
    print(str(kilo_bytes)," kilobytes")
    kibi_bytes=number/1024
    print(str(kibi_bytes),' kibibytes')
    mega_bytes=kilo_bytes//1000
    print(str(mega_bytes), " megabytes")
    mebi_bytes=kibi_bytes/1024
    print(str(mebi_bytes), ' mebibytes')

def main():
    number=input("Please enter a number:")
    units_converter(int(number))

if __name__=='__main__':
    main()