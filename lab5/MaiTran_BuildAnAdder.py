from scipy.stats import norm

# Find the z-score for P(Z < z) = 0.99
z = norm.ppf(0.99)
print("The z-score for P(Z < z) = 0.99 is:", z)
