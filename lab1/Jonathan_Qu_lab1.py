#Decimal to binary function
def decimal_to_binary(number: int) -> str:
    """
    This is a comment
    Input: Number (integer)
    Output: String
    Example: Input = 43605, Output = "0xAA55"
    """
    answer = ""
    
    #Error message if the number is negative
    if (number < 0):
        print("Error! Positive numbers only!")
    # Forever loop
    while number > 0:
        # Integer divide using the // operator
        quotient = number // 2
        # Get the remainder using the % operator
        remainder = number % 2
        
        # Accumulate result
        answer = str(remainder) + answer

        # Set the number we need to use for next time
        number = quotient
        
        # We break the "loop" when division turns to zero
        if (quotient == 0):
            break
    return "0b" + answer #returns answer

print(decimal_to_binary(123)) #Prints decimal to binary
print(decimal_to_binary(0)) #Prints decimal to binary
print(decimal_to_binary(-12)) #Prints error message

