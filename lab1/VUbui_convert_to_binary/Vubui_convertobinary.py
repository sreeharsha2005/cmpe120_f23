
def to_binary(number: int) -> str:
    """
    This is a comment
    Input: Number (integer)
    Output: String
    Example: Input = 17, Output = "0b10001"
    """
    # Handle the special case when the input number is 0
    if number == 0:
        return "0b0"

    answer = ""
    
    while True:
        # Get the remainder using the % operator
        remainder = number % 2
        
        # Accumulate result
        answer = str(remainder) + answer

        # Integer divide using the // operator
        quotient = number // 2

        # Set the number we need to use for the next iteration
        number = quotient
        
        # We break the loop when division turns to zero
        if quotient == 0:
            break
    
    return "0b" + answer


print(to_binary(255))   # Output: "0b11111111"
print(to_binary(11))     # Output: "0b1011"
print(to_binary(2087))     # Output: "0b100000100111"
