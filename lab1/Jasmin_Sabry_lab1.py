#Name: Jasmin Sabry

def nibble_to_ascii(nibble: int) -> str:
    table = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    return table[nibble]

def to_hex(number: int) -> str:
    answer = ""
    while True:
        # divide
        quotient = number // 16
        # Get the remainder
        remainder = number % 16

        # Get result
        answer = nibble_to_ascii(remainder) + answer

        number = quotient

        # Break the loop when division turns to zero
        if (quotient == 0):
            break

    return "0x" + answer


def to_binary(number: int) -> str:
    answer = ""
    while True:
        # divide
        quotient = number // 2
        # Get the remainder
        remainder = number % 2

        # Get result
        answer = nibble_to_ascii(remainder) + answer

        number = quotient

        # Break the loop when == 0
        if (quotient == 0):
            break

    return "0b" + answer


# Test
print(to_binary(14))
print(to_binary(0b1101010))
print(to_binary(0xDA))