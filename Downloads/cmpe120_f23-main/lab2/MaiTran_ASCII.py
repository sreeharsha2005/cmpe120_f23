
#Python-ASCII
# a function to return the uppercase version of a letter
def upper_letter(letter):
    if 'a' <= letter <= 'z':
        upper_ascii = ord(letter) - ord('a') + ord('A')
        return chr(upper_ascii)
    else:
        return letter
    
answer = upper_letter('a')
print(answer)

#a function to return the lowercase version of a letter
def lower_letter(letter):
    if 'A' <= letter <= 'Z':
        return chr(ord(letter)-ord('A')+ord('a'))
    else:
        return letter
    
answer = lower_letter('A')
print(answer)

#a function that returns true if the letter is an alphabet
def is_alphabet(letter):
    if 'a' <= letter <= 'z' or 'A' <= letter <= 'Z':

        return True
    else:
        return False
answer1 = 'A'
answer2 = 'f'
answer3 = '3'

print(answer1.isalpha())
print(answer2.isalpha())
print(answer3.isalpha())

#a function that returns true if an element is a digit
def is_digit(element):
    if '0' <= element <= '9':
        return True
    else:
        return False
answer1 = '2'  
answer2 = '@' 
answer3 = 't'

print(answer1.isdigit())
print(answer2.isdigit())
print(answer3.isdigit())

#a function to determine if the given character is a special character or not
message = '$ymbol'
from string import ascii_letters, digits
if set(message).difference(ascii_letters + digits):
    print('Message has special characters!')
else:
    print("Message has not special characters!")


    


