#Function returns the uppercase and lowercase of a letter
def upper_lower(letter):
    #The function ord() gets the ascii
    letter_in_ascii = ord(letter)
    #If the ascii is between 97-122 then subtracts 32
    if letter_in_ascii in range (97,123):
        letter_in_ascii -= 32
        return chr(letter_in_ascii)
    #else if the ascii is between 65-90 then adds 32
    elif letter_in_ascii in range (65,91):
            letter_in_ascii += 32
            return chr(letter_in_ascii)
print(upper_lower('a')) #prints out the uppercase
print(upper_lower('B')) #prints out the lowercase
print(upper_lower('z')) #prints out the uppercase

#Function returns true if the letter is an alphabet
def alphabet_check(letter):
    letter_in_ascii = ord(letter)
    #If the ascii is between 97-122 or 65-90 returns true
    if letter_in_ascii in range (97,123) or letter_in_ascii in range(65,91):
            return True
    else:
        #returns false if not within range
        return False
print("\nAlphabet check")
print (alphabet_check('1')) #prints false
print (alphabet_check('A')) #prints true
print (alphabet_check('j')) #prints true

#Function returns true if an element is a digit
def digit_check(digit):
    digit_in_ascii = ord(digit)
    #if the ascii is between 48-57 returns true
    if digit_in_ascii in range (48,58):
            return True
    else:
        #returns false if not within range
        return False
print("\nDigit check")
print (digit_check('9')) #returns true
print (digit_check('A')) #retuns false
print (digit_check('j')) #returns false

"Function determines if the given character is a special character or not"
def special_char_check(digit):
    sc_ascii = ord(digit)
    #if the ascii is within these range it will return true
    if sc_ascii in range (33,48) or sc_ascii in range (58,65) or sc_ascii in range (91,97):
            return True
    else:
    #returns false if not in range
        return False
print("\nSpecial character check")
print (special_char_check('!')) #returns true
print (special_char_check('d')) #returns false
print (special_char_check(':')) #returns true
print (special_char_check('^')) #returns true
