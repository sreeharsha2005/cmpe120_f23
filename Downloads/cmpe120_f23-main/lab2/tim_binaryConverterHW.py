def convertToBinary(num) ->str:
    ans = "" #empty answer, will concatenate over time
    if num == 0: #check if int is zero, automatically return 0 (0 in binary is 0)
        return "0b" + "0"
    elif num < 0: #negative number cannot be converted to binary, return error message
        return "Unable to convert to binary. Please choose a positive number."
    else:
        while num > 0: #while loop for calculating conversion to binary through remainder method
            remainder = num % 2
            quotient = num // 2
            ans = str(remainder) + ans #adding the remainder to the back of the number
            num = quotient #updating value of num to be the quotient of it divided by 2 (to the nearest whole number)
    return "0b" + ans #returning the answer
print(convertToBinary(15)) #returns 0b1111
print(convertToBinary(79)) #returns 0b1001111
print(convertToBinary(0)) #returns 0b0
print(convertToBinary(98)) #returns 0b1100010
print(convertToBinary(2271)) #returns 0b100011011111
print(convertToBinary(-1)) #returns error message since it is negative