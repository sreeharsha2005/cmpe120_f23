''' HW: Python Binary Converter - Alvin Dang - 9/18/2023

    Modify the Hex converter program to convert a number to binary
        Follow the hex converter example
        Modify as necessary and test
'''
def to_binary(number: int) -> str:
    answer = ""
    
    # Forever loop
    while True:
        # Integer divide using the // operator
        quotient = number // 2
        # Get the remainder using the % operator
        remainder = number % 2
        
        # Accumulate result
        answer = str(remainder) + answer

        # Set the number we need to use for next time
        number = quotient
        
        # We break the "loop" when division turns to zero
        if (quotient == 0):
            break
    
    return "0b" + answer

#   Personal Tests
def test_to_binary():
    # Test Decimal
    assert to_binary(123) == "0b1111011"
    assert to_binary(0) == "0b0"
    assert to_binary(29) == "0b11101"

    # Test Binary
    assert to_binary(0b1010) == "0b1010"
    assert to_binary(0b0) == "0b0"

    # Test Hexadecimal
    assert to_binary(0xFEED) == "0b1111111011101101"
    assert to_binary(0x0) == "0b0"
    assert to_binary(0xBEADDEEF) == "0b10111110101011011101111011101111"

    print("All Binary tests passed!")

# Run tests
print(to_binary(123))
print(to_binary(0b1010))
print(to_binary(0xFEED))
test_to_binary()