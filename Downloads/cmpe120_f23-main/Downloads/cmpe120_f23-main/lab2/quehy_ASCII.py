#HW Python - ASCII

# Write a function to return the uppercase version of a letter
def uppercase_letter(letter):
    # Check if the input is a single character and lowercase letter between 'a' and 'z'
    if len(letter) == 1 and 'a' <= letter <= 'z':
        # Convert to uppercase using ASCII values
        uppercase_ascii = ord(letter) - ord('a') + ord('A')
        # Return the letter
        return chr(uppercase_ascii)
    else:
        return letter
# Print answer  
print ("Test case 1 return to uppercase letter")     
print (uppercase_letter('a'))
print (uppercase_letter('q'))


# Write a function to return the lowercase version of a letter
def lowercase_letter(letter):
    # Check if the input is a single character and uppercase between 'a' to 'z'
    if len(letter) == 1 and 'A' <= letter <= 'Z':
        # Convert to lowercase using ASCII values
        lowercase_ascii = ord (letter) - ord('A') +ord('a')
        # Return the letter
        return chr(lowercase_ascii)
    else:
        return letter 

# Print answer
print ("Test case 2 return to lowercase letter")   
print (lowercase_letter('Q'))
print (lowercase_letter('N'))


# Write a function that returns true if the letter is an alphabet 
def is_alphabet(letter):
    # Get the ASCII 
    checkletter_ascii = ord (letter)
    # Check if the input is an alphabet letter
    if ('a' <= checkletter_ascii <= 'z') or ('A' <= checkletter_ascii <= 'Z'):
        # If the input is an alphabet letter, the result is True
        return True 
    else: 
        # If not, the result is False
        return False

# Put the answer to check it
answer_1 = 'n'
answer_2 = 'B'
answer_3 = '3'

# Print the result
print ("Test case 3 return true if the letter is alphabet")   
print(answer_1.isalpha())
print(answer_2.isalpha())
print(answer_3.isalpha())


# Write a function that returns true if an element is a digit 
def is_digit(element):
    # Get the ASCII 
    checkdigit_ascii = ord (element)
    # Check if the input is a digit
    if ('60' <= checkdigit_ascii <= '69'):
        # If the input is a digit, the result is True
        return True 
    else: 
        # If not, the result is False
        return False

# Put the answer to check it
answer_1 = 'n'
answer_2 = 'B'
answer_3 = '65'

# Print the result
print ("Test case 4 return true if an element is a digit") 
print(answer_1.isdigit())
print(answer_2.isdigit())
print(answer_3.isdigit())


# Write a function to determine if the given character is a special character or not 
def check_special_char_ascii(string):
    # Initialize a for loop that iterates through each character in the input string.
    for char in string:
        # Check if the ASCII value is in the range of special characters
        if ord(char) < 48 or (57 < ord(char) < 65) or (90 < ord(char) < 97) or ord(char) > 122:
            # If the input is special character, the result is True
            return "This is a special character."
    # If not, the result is False
    return "This is NOT a special character."
 
# Put the answer to check 
string1 = "2509"
string2 = "%"
string3 = "B"

# Print the result
print ("Test case 5 return true if the given character is a special or not")
print(check_special_char_ascii(string1))
print(check_special_char_ascii(string2))
print(check_special_char_ascii(string3))