#print ("Hello World!")
#python binary converter
def decimal_to_binary(num):
    binary_num = ''
    if(num == 0): # if we put 0 it will give 0b0
        return "0b0"
    if (num < 0): # if we put negative number it will give positive number only
        return "Positive Number Only"
    while(num >= 1): #loop while 
        quotient = num //2 # integer divide using // 
        remainder = num % 2 # get remainder using % then it will add to the result
        binary_num = str(remainder) + binary_num # accumulate result
        
        num = quotient # set the num to quotient so we can use for next time
    
    return '0b' + binary_num # return number to binary 

print(decimal_to_binary(25)) # print '0b11001'
print(decimal_to_binary(0)) # print '0b0'
print(decimal_to_binary(-11))# print Positive Number Only 