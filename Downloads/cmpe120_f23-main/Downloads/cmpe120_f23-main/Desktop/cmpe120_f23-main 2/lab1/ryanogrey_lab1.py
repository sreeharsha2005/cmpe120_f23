def convertToBin(num):
    solution = ""

    #binary only represents positive decimals
    if num < 0:
        solution = "Only input positive decimals"
        return solution

    #Base case. 0 in binary is 0b0
    if num == 0:
        solution = "0b0"
        return solution

    #Continues dividing down until reaching 1
    while(num >= 1):
        #Constructing the binary value similiar to a stack (last in first out)
        #As a result, the previous remainders are appended to the most recent remainder
        solution = str(num % 2) + solution
        #taking floor value of division
        num = num // 2
    
    #Returning solution with 0b notation
    return "0b" + solution

#testing solution: inputting 25 should result in 0b11001
print(convertToBin(25))

#testing solution: inputting 0 should result in 0b0
print(convertToBin(0))

#testing solution: inputting -5 should result in a String message
print(convertToBin(-5))