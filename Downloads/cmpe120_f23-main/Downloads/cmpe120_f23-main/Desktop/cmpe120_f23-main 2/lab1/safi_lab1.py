# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.

# function to convert a decimal number to binary

def decimal_to_bin(num):

    if (num == 0):
        return 0
    if (num < 0):
        return "Please enter positive numbers"
    bin = ''

    while (num >= 1):
        quotient = num // 2
        rem = num % 2
        bin = str(rem) + bin
        num = quotient
    return bin


res = decimal_to_bin(8)
print(res)
