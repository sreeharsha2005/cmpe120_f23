class lab3:
   answer=0
   def units_converter(self,number):
      print("Conversions:")
      print(str(number)," bytes")
      kilo_bytes=round(number/1000,4)
      print(str(kilo_bytes)," kilobytes")
      kibi_bytes=round(number/1024,4)
      print(str(kibi_bytes),' kibibytes')
      mega_bytes=round(kilo_bytes//1000,4)
      print(str(mega_bytes), " megabytes")
      mebi_bytes=round(kibi_bytes/1024,4)
      print(str(mebi_bytes), ' mebibytes')

   def question(self):
       try:
          self.answer = int(input("Please enter an integer:"))
       except ValueError:
          print("it is not an integer")
          self.answer=self.question()

       if self.answer<0:
          print("please enter a positive input,input is smaller than 0")
          return self.question()
       else:
          return self.answer

def main():
    test=lab3()
    number=test.question()
    test.units_converter(number)


if __name__=='__main__':
    main()