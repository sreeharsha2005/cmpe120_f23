#Creating a map so that the converstion can be O(1) and easy.

char_map = {
    'a': 'A', 'b': 'B', 'c': 'C', 'd': 'D', 'e': 'E',
    'f': 'F', 'g': 'G', 'h': 'H', 'i': 'I', 'j': 'J',
    'k': 'K', 'l': 'L', 'm': 'M', 'n': 'N', 'o': 'O',
    'p': 'P', 'q': 'Q', 'r': 'R', 's': 'S', 't': 'T',
    'u': 'U', 'v': 'V', 'w': 'W', 'x': 'X', 'y': 'Y',
    'z': 'Z'
}
#Uppercase converter
def uppercase_converter(char):
    if 'a' <= char <= 'z':
        return char_map[char]
    else:
        return char

# i = input()
# res = uppercase_converter(i)
# print(res)

#Lowercase converter
def lowercase_converter(char):
    #It is a little bit more work to retrieve the key with a given value from a map.
    if 'A' <= char <= 'Z':
        value_list = list(char_map.values()) #Having a list of values from map
        key_list = list(char_map.keys()) #Having a list of keys from map
        return key_list[value_list.index(char)] #Use indexing to locate the value's index in the key list
    else:
        return char

#i = input()
#res = lowercase_converter(i)
#print(res)

#isLetter
def is_letter(char):
    #Checking the range of the given char. If anything other than alphabet (ASCII), return false
    return 'A' <= char <= 'Z' or 'a' <= char <= 'z'

# i = input()
# res = is_letter(i)
# print(res)

#is_digit
def is_digit(char):
    #Same idea as is_letter()
    return '0' <= char <= '9'

#i = input()
#res = is_digit(i)
#print(res)

#is_special
def is_special(char):

    print(ord(char), ord('A'), ord('z')) #Use to see what will ord() return
    # if('0' <= char <= '9' or 'A' <= char <= 'Z' or 'a' <= char <= 'z'):
    # Line 58 was original code. As I was adding comments, I have the idea to reuse 
    # the functions is_digit() and is_letter
    if(is_digit(char) or is_letter(char)):
        print("Not a special character")
    else:
        print("Is a special character")

i = input()
is_special(i)