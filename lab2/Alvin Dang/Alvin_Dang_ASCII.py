''' HW: Python ASCII - Alvin Dang - 9/18/2023

    Do not use built-in functions relevant to the conversion
        For instance, you should not use an existing .to_lower function or similar
        But it is okay to use other non relevant built-in functions such as print
'''

#   Write a function to return the uppercase version of a letter
def uppercase_letter(letter):
    # Check if letter is lowercase
    if 'a' <= letter <= 'z':
        # Convert letter to uppercase and return
        return chr(ord(letter) - 32)
    # Return letter if it is not lowercase
    else:
        return letter
    
#   Write a function to return the lowercase version of a letter
def lowercase_letter(letter):
    # Check if letter is uppercase
    if 'A' <= letter <= 'Z':
        # Convert letter to lowercase and return
        return chr(ord(letter) + 32)
    # Return letter if it is not uppercase
    else:
        return letter
    
#   Write a function that returns true if the letter is an alphabet
def is_alphabet(letter):
    # Check for edge case (Empty string)
    if letter == '':
        return False

    # Ternary Operator! Return true if letter is an alphabet, false otherwise
    return (True if ((65 <= ord(letter) <= 90) or (97 <= ord(letter) <= 122)) else False)

#   Write a function that returns true if an element is a digit
def is_digit(element):
    # Check for edge case (Empty string)
    if element == '':
        return False

    # Ternary Operator! Return true if element is a digit, false otherwise
    return (True if (48 <= ord(element) <= 57) else False)

#   Write a function to determine if the given character is a special character or not
def is_special_char(char):
    # Check for edge case (Empty string)
    if char == '':
        return False

    # Ternary Operator! Return true if char is a special character, false otherwise
    return (True if ((32 <= ord(char) <= 47) or (58 <= ord(char) <= 64) or (91 <= ord(char) <= 96) or (123 <= ord(char) <= 126)) else False)

#   Test
def test_uppercase_letter():
    # Test lowercase letters
    assert uppercase_letter('a') == 'A'
    assert uppercase_letter('z') == 'Z'

    # Test assorted cases (should return the input unchanged)
    assert uppercase_letter('G') == 'G'
    assert uppercase_letter('7') == '7'
    assert uppercase_letter('$') == '$'
    assert uppercase_letter(' ') == ' '

    # Test edge case (empty string)
    assert uppercase_letter('') == ''
    
    print("All uppercase test cases passed!")

def test_lowercase_letter():
    #Test uppercase letters
    assert lowercase_letter('A') == 'a'
    assert lowercase_letter('Z') == 'z'

    # Test assorted cases (should return the input unchanged)
    assert lowercase_letter('g') == 'g'
    assert lowercase_letter('7') == '7'
    assert lowercase_letter('$') == '$'
    assert lowercase_letter(' ') == ' '

    # Test edge case (empty string)
    assert lowercase_letter('') == ''

    print("All lowercase test cases passed!")

def test_is_alphabet():
    # Test letters
    assert is_alphabet('Q') == True
    assert is_alphabet('A') == True
    assert is_alphabet('z') == True
    assert is_alphabet('s') == True

    # Test numbers
    assert is_alphabet('1') == False
    assert is_alphabet('0') == False
    assert is_alphabet('9') == False
    
    # Test special characters
    assert is_alphabet('$') == False
    assert is_alphabet(' ') == False

    # Test edge case (empty string)
    assert is_alphabet('') == False

    print("All is_alphabet test cases passed!")

def test_is_digit():
    # Test numbers
    assert is_digit('1') == True
    assert is_digit('0') == True
    assert is_digit('9') == True

    # Test letters
    assert is_digit('z') == False
    assert is_digit('A') == False

    # Test special characters
    assert is_digit('$') == False
    assert is_digit(' ') == False

    # Test edge case (empty string)
    assert is_digit('') == False

    print("All is_digit test cases passed!")

def test_is_special_char():
    # Test special characters
    assert is_special_char('$') == True
    assert is_special_char(' ') == True
    assert is_special_char('!') == True

    # Test letters
    assert is_special_char('z') == False
    assert is_special_char('A') == False

    # Test numbers
    assert is_special_char('1') == False
    assert is_special_char('0') == False
    assert is_special_char('9') == False

    # Test edge case (empty string)
    assert is_special_char('') == False

    print("All is_special_char test cases passed!")

# Run the test cases
test_uppercase_letter()
test_lowercase_letter()
test_is_alphabet()
test_is_digit()
test_is_special_char()