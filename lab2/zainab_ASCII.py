# this function returns true if the letter is an alphabet
def alphabet(char):
  """
  Input: 'r', Output: True
  Input: 8, Ouput: False
  
  """

  alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  for letter in alphabet:
    if char == letter:
      return True
  return False





#This function returns true if an element is a digit
def digit(char):
  """
  Input: 8, Output: True
  Input: 'a', Ouput: False
  
  """
  digits = "0123456789"
  for digit in digits:
    if char == digit:
      return True
  return False





#this function determines if the given character is a special character or not
def specialChar(char):
  """
  Input: %, Output: "The given character is a   special character." 
  Input: 5, Output: "The given c  harcter is NOT a special
  character."
  Input = R, Output: "The given charcter is NOT a special character."
  
  """
  
  if alphabet(char) or digit(char):
    return ("The given charcter is NOT a special character.")
  else:
    return ("The given character is a special character.")





#this function returns the uppercase version of a letter
def uppercase(char): 
  """
  input: r, output: R
  input: R, output R
  
  """
  if alphabet(char):
    uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    lowercase = "abcdefghijklmnopqrstuvwxyz"
    if char in uppercase:
        return char
    index = lowercase.find(char)
    return uppercase[index]






#this function returns the lowercase version of a letter
def lowercase(char):
  """
  input: q, output: Q 
  input: q, output: q
  """
  if alphabet(char):
    uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    lowercase = "abcdefghijklmnopqrstuvwxyz"
    if char in lowercase:
      return char
    index = uppercase.find(char)
    return lowercase[index]

    



print(alphabet('S'))
print(digit('1'))
print(specialChar('%'))
print(uppercase('r'))
print(lowercase('Q'))