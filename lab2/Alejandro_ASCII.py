# 1) Write a function to return the uppercase version of a letter
def toUppercaseLetter(letter):
    if ord(letter) > 122 or ord(letter) < 97: # check to ensure range is within lower case ASCII letters
        print("Unable to convert letter to upper case. Enter a new input.") #ask user to enter new input
    return chr(ord(letter) - 32) #using ord(num) - 32 returns the decimal representation of the letter in the uppercase form, chr() will return the decimal representaion as a character

# 2) Write a function to return the lowercase version of a letter
def toLowercaseLetter(letter):
    if ord(letter) > 90 or ord(letter) < 65: # check to ensure range is within upper case ASCII letters
        print("Unable to convert letter to lower case. Entet a new input.") #ask user to enter new input
    return chr(ord(letter) + 32) # using ord(num) + 32 returns the decimal representation of the letter in the lowercase format, chr() will return the decimal reprensentaion as a character

# 3) Write a function that returns true if the letter is an alphabet
def isAlphabet(num):
    if ord(num) < 65: # characters with a decimal representation under 65 are special characters, that is not part of the alphabet
        return False
    elif ord(num) > 90 and ord(num) < 97: # the characters that are in between the upper and lower case are special characters
        return False
    elif ord(num) > 122: #all the numbers that are past decimal represenation 122 are special characters
        return False
    else:
        return True

# 4) Write a function that returns true if an element is a digit
def isDigit(num):
    if ord(num) >= 48 and ord(num) <= 57: # the digits 0-9 are represented in decimal represenation from 48-57
        return True
    else:
        return False

# 5) Write a function to determine if the given character is a special character or not
def isSpecial(char):
    if ord(char) > 47 and ord(char) < 58: # take out the digit values 0-9 (decimal representation)
        return False
    elif ord(char) > 64 and ord(char) < 91: # take out the upper case letters (decimal representation)
        return False
    elif ord(char) > 96 and ord(char) < 123: # take out the lower case letters (decimal representation)
        return False
    else:
        return True
    
# Testing to make sure all of the functions work properly
print("Testing toUppercaseLetter function (a -> A): ", end="")
print(toUppercaseLetter('a')) # a -> A
print("Testing toLowercaseLetter function (A -> a): ", end="")
print(toLowercaseLetter('A')) # A -> a
print("Testing isAlphabet (B): ", end="")
print(isAlphabet('B')) 
print("Testing isAlphabet (9): ", end="")
print(isAlphabet('9'))
print("Testing isDigit (7): ", end="")
print(isDigit('7'))
print("Testing isDigit (A): ", end="")
print(isDigit('A'))
print("Testing isSpecial ($): ", end="")
print(isSpecial('$'))
print("Testing isSpecial (G): ", end="")
print(isSpecial('G'))