#HW: Python Binary Converter

def to_bin(number: int) -> str:
    """
    This is a comment
    Input: Number (integer)
    Output: String
    Example: Input = 43605, Output = "0b1010101001010101"
    """
    answer = ""
    
    # Forever loop
    while True:
        # Integer divide using the // operator
        quotient = number // 2
        # Get the remainder using the % operator
        remainder = number % 2
        
        # Accumulate result
        answer = str(remainder) + answer

        # Set the number we need to use for next time
        number = quotient
        
        # We break the "loop" when division turns to zero
        if (quotient == 0):
            break
    
    return "0b" + answer

print(to_bin(43605))